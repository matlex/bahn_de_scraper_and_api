from flask import Flask, request, jsonify
from scraper import Scraper

app = Flask(__name__)


@app.route('/', methods=['POST'])
def check_reservation_possibility():
    scraper = Scraper(request_payload=request.json)
    try:
        result = scraper.process_reservation_possibility()
    except Exception as e:
        result = {"error": e.message}
    return jsonify(result)
