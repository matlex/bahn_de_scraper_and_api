# *-* coding:utf-8 *-*
from datetime import datetime
from grab import Grab
from lxml.html import fromstring

FROM_STATION_FIELD_ID = 'locS0'
TO_STATION_FIELD_ID = 'locZ0'

# Outward/departure journey fields
OUTWARD_JOURNEY_DATE_FIELD_ID = 'REQ0JourneyDate'  # value="02.12.16"
OUTWARD_JOURNEY_TIME_FIELD_ID = 'REQ0JourneyTime'  # value="11:20"

OUTWARD_JORNEY_DEPARTURE_BUTTON_ID = 'REQ0HafasSearchForw1'  # value="1"
OUTWARD_JORNEY_ARRIVAL_BUTTON_ID = 'REQ0HafasSearchForw0'  # value="0"

# Return/arrival journey fields
INBOUND_JOURNEY_DATE_FIELD_ID = 'REQ1JourneyDate'  # value="02.12.16"
INBOUND_JOURNEY_TIME_FIELD_ID = 'REQ1JourneyTime'  # value="11:20"

INBOUND_JOURNEY_DEPARTURE_BUTTON_ID = 'REQ1HafasSearchForw1'  # value="1"
INBOUND_JOURNEY_ARRIVAL_BUTTON_ID = 'REQ1HafasSearchForw0'  # value="0"

NUMBER_OF_TRAVELERS_ID = 'traveller_Nr'
TRAVELER_TYPE_ID = 'travellerType_{}'  # Add number of traveler seat
''' SAMPLE travellerType_ HTML CODE
<select name="REQ0Tariff_TravellerType.2" id="travellerType_2">
<option value="E">1 Adult</option>
<option value="F">child 6-14 years.</option>
<option value="B">child 0-5 years</option>
</select>
'''
TARIFF_CLASS_ID1 = 'REQ0Tariff_Class_1'  # <inpu type="radio" name="REQ0Tariff_Class" value="1" id="REQ0Tariff_Class_1">
TARIFF_CLASS_ID2 = 'REQ0Tariff_Class_2'  # <inpu type="radio" name="REQ0Tariff_Class" value="2" id="REQ0Tariff_Class_2">

SUBMIT_BUTTON_ID = 'searchConnectionButton'


class Scraper:
    def __init__(self, request_payload):
        self._INITIAL_SEARCH_URL = 'https://reiseauskunft.bahn.de/bin/query.exe/en?&rit=yes&country=DEU'
        self.g = Grab()
        self.request_payload = request_payload
        self.round_trip_journey = False
        self.validate_request_payload_before_search()

    def validate_request_payload_before_search(self):
        """
        Processing and validating input request_payload. Checks for various combinations of data.
        :return:
        """
        if self.request_payload['inbound_arrival'] and self.request_payload['inbound_departure']:
            raise AssertionError("inbound_arrival and inbound_departure can't be in the same request")

        elif self.request_payload['outbound_arrival'] and self.request_payload['outbound_departure']:
            raise AssertionError("outbound_arrival and outbound_departure can't be in the same request")

        elif not self.request_payload['arr_ident'] or not self.request_payload['dep_ident']:
            raise AssertionError("arr_ident and dep_ident must be filled")

        elif int(self.request_payload['passengers']) > 5 or int(self.request_payload['passengers']) < 1:
            raise AssertionError("'passengers' field can be from 1 to 5")

        elif self.request_payload['class'] != ("2" or "1"):
            raise AssertionError("'class' field can be from either 1 or 2")

        # Check whether request is for round_trip_journey or not
        if (self.request_payload['outbound_arrival'] or self.request_payload['outbound_departure']) and \
                (self.request_payload['inbound_arrival'] or self.request_payload['inbound_departure']):
            self.round_trip_journey = True

    def return_datetime_from_string(self, string):
        """
        Accepts datetime string in format 'DDMMYYhhmm' eg."0312161255", where date is 03.12.16 and time 12:55
        :param string:
        :return: {'date': 'dd.mm.yy', 'time': 'HH:MM'}
        """
        return {
            'date': datetime.strptime(string, "%d%m%y%H%M").strftime('%d.%m.%y'),
            'time': datetime.strptime(string, "%d%m%y%H%M").strftime('%H:%M')
        }

    def prepare_and_run_search_trips_request(self):
        """
        Fills in fields in form, makes query(submits form) and returns response
        :return: html_source_code after submitting
        """
        self.g.go(self._INITIAL_SEARCH_URL)
        self.g.doc.set_input_by_id(FROM_STATION_FIELD_ID, self.request_payload['dep_ident'])
        self.g.doc.set_input_by_id(TO_STATION_FIELD_ID, self.request_payload['arr_ident'])

        # Outbound/Departure fields filling
        if self.request_payload['outbound_departure']:
            self.g.doc.set_input_by_id(OUTWARD_JOURNEY_DATE_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['outbound_departure'])[
                                           'date'])
            self.g.doc.set_input_by_id(OUTWARD_JOURNEY_TIME_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['outbound_departure'])[
                                           'time'])
            self.g.doc.set_input_by_id(OUTWARD_JORNEY_DEPARTURE_BUTTON_ID, "1")
        if self.request_payload['outbound_arrival']:
            self.g.doc.set_input_by_id(OUTWARD_JOURNEY_DATE_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['outbound_arrival'])[
                                           'date'])
            self.g.doc.set_input_by_id(OUTWARD_JOURNEY_TIME_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['outbound_arrival'])[
                                           'time'])
            self.g.doc.set_input_by_id(OUTWARD_JORNEY_ARRIVAL_BUTTON_ID, "0")

        # Inbound/Arrival fields filling
        if self.request_payload['inbound_departure']:
            self.g.doc.set_input_by_id(INBOUND_JOURNEY_DATE_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['inbound_departure'])[
                                           'date'])
            self.g.doc.set_input_by_id(INBOUND_JOURNEY_TIME_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['inbound_departure'])[
                                           'time'])
            self.g.doc.set_input_by_id(INBOUND_JOURNEY_DEPARTURE_BUTTON_ID, "1")
        if self.request_payload['inbound_arrival']:
            self.g.doc.set_input_by_id(INBOUND_JOURNEY_DATE_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['inbound_arrival'])[
                                           'date'])
            self.g.doc.set_input_by_id(INBOUND_JOURNEY_TIME_FIELD_ID,
                                       self.return_datetime_from_string(self.request_payload['inbound_arrival'])[
                                           'time'])
            self.g.doc.set_input_by_id(INBOUND_JOURNEY_ARRIVAL_BUTTON_ID, "0")

        # Processing Travelers/Passengers
        # <option value="1" selected="selected">1 Traveller</option> max 5
        self.g.doc.set_input_by_id(NUMBER_OF_TRAVELERS_ID, self.request_payload['passengers'])
        # If traveller_Nr == 1 then TRAVELER_TYPE_ID will be just travellerType_1
        # If traveller_Nr == 2 then TRAVELER_TYPE_IDs will be travellerType_1, travellerType_2 and so on till 5
        for traveler_number in range(1, int(self.request_payload['passengers']) + 1):
            self.g.doc.set_input_by_id(TRAVELER_TYPE_ID.format(str(traveler_number)), "E")

        # Processing Tariff Class
        if self.request_payload["class"] == '2':
            self.g.doc.set_input_by_id(TARIFF_CLASS_ID2, "2")
        elif self.request_payload["class"] == '1':
            self.g.doc.set_input_by_id(TARIFF_CLASS_ID1, "1")

        # Submit filled form to server
        self.g.doc.submit(submit_name="start")

        return self.g.doc.body

    def get_reservation_or_return_journey_link_from_trip_results(self, search_trip_results_html):
        """
        Finds and extracts 'Reservation' or 'Return journey' link from trip results table
        :param search_trip_results_html: It is a g.doc.body
        :return: 'some_http_url_string'
        """
        # Now we have to find a correct trip and click in "Reservation" button
        parsed_html = fromstring(search_trip_results_html)
        trip_results = parsed_html.xpath('//*[@class="boxShadow "]')
        for trip_result_row in trip_results:
            try:
                reservation_link = trip_result_row.xpath('*/td[@class="fareStd button-inside tablebutton center"]//a')[
                    0]
                return reservation_link.xpath('@href')[0]
            except IndexError:
                # If not found it raises IndexError exception. Then continue to next row.
                continue
        # If found nothing raise exception
        raise AssertionError("there are no any 'reservation' or return 'journey' link on the search results page. "
                             "check your request data")

    def get_seat_reservation_page_without_registration(self, reservation_link):
        """
        Opens a reservation_link, then fill in checkbox 'Book without registering' and submits form.
        :param reservation_link:
        :return: html_source_code
        """
        self.g.go(reservation_link)
        # Process first intermediate page
        self.g.doc.set_input_by_id('nameRadioGroupLogin3', "3")
        self.g.doc.submit(submit_name='button.weiter')
        # Process second intermediate page
        self.g.doc.submit(submit_name='button.weiter')
        return self.g.doc.body

    def check_reservation_possibility(self, seat_reservation_html):
        """
        If request was for outward journey then just search falling conditions along whole html,
        but if there was request for outward and return journey then there will be two separate tables with possible
        separate results. So we have to handle each direction separately.
        """
        if not self.round_trip_journey:  # If just an outward journey
            if 'All seats are already fully booked' in seat_reservation_html and \
                            'class="inputtabelle withicon bgIconInactive"' in seat_reservation_html:
                return {'outbound_available': False}
            else:
                return {'outbound_available': True}
        elif self.round_trip_journey:  # We have both outward and return journey tables
            output = {}
            parsed_html = fromstring(seat_reservation_html)

            # Look for relative line with the header 'Return journey'.
            # All of it above refers to the 'Outward', everything below - to 'Return'.

            # Find first table results - Outward journey
            # Search all nodes before context node + preceding::*
            before_return_jorney_row = parsed_html.xpath(
                '//*[@id="reswunschtable"]//tr[@class="rowlines"][3]/preceding::tr[@class="inputtabellelight"]')
            try:
                for inputtabellelight in before_return_jorney_row:
                    # If 'bgIconInactive' is found then break loop
                    x_found = inputtabellelight.xpath('//*[@class="inputtabelle withicon bgIconInactive"]')[0]
                    output['outbound_available'] = False
                    break
            except IndexError:
                output['outbound_available'] = True

            # Find second table results - 'Return journey'
            # Search all nodes after context node + following::*
            after_return_jorney_row = parsed_html.xpath(
                '//*[@id="reswunschtable"]//tr[@class="rowlines"][3]/following::tr[@class="inputtabellelight"]')
            try:
                for inputtabellelight in after_return_jorney_row:
                    # If 'bgIconInactive' is found then break loop
                    x_found = inputtabellelight.xpath('//*[@class="inputtabelle withicon bgIconInactive"]')[0]
                    output['inbound_available'] = False
                    break
            except IndexError:
                output['inbound_available'] = True

            return output

    def process_reservation_possibility(self):
        """
        The main process function. Processes search request process and all data extracting with returning results.
        :return: {'outbound_available': True/False} or
        {'outbound_available': True/False, 'inbound_available': True/False}
        """
        # If we have a round trip then we should find "Return Journey" link and then "Reservation" link and continue.
        if (self.request_payload['outbound_departure'] or self.request_payload['outbound_arrival']) and (
                    self.request_payload['inbound_departure'] or self.request_payload['inbound_arrival']):
            search_trip_results_html = self.prepare_and_run_search_trips_request()
            return_journey_link = self.get_reservation_or_return_journey_link_from_trip_results(
                search_trip_results_html)
            return_journey_trip_results_html = self.g.go(return_journey_link).body
            reservation_link = self.get_reservation_or_return_journey_link_from_trip_results(
                return_journey_trip_results_html)
            seat_reservation_html = self.get_seat_reservation_page_without_registration(reservation_link)
            reservation_is_possible = self.check_reservation_possibility(seat_reservation_html)
        else:  # Otherwise find just "Reservation" link and continue the process
            search_trip_results_html = self.prepare_and_run_search_trips_request()
            reservation_link = self.get_reservation_or_return_journey_link_from_trip_results(search_trip_results_html)
            seat_reservation_html = self.get_seat_reservation_page_without_registration(reservation_link)
            reservation_is_possible = self.check_reservation_possibility(seat_reservation_html)
        return reservation_is_possible


if __name__ == '__main__':
    mock_request_payload = {
        "arr_ident": "8000001",
        "dep_ident": "8000261",
        "class": "2",
        "inbound_arrival": "",
        "inbound_departure": "",
        "outbound_arrival": "",
        "outbound_departure": "0312161200",
        "passengers": "1"
    }
    scraper = Scraper(request_payload=mock_request_payload)
    result = scraper.process_reservation_possibility()
    print result
